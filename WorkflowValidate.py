import sys, os
sys.path.extend(['.','..', '../..'])

import mupif as mp
from mupif.units import U as u
import logging
log = logging.getLogger()

from ModelGrainStateFromPDB import GrainStateFromPDB
from ModelCreatePDB import CreatePdb
from validate import *


class WorkflowValidate(mp.workflow.Workflow):
   
    def __init__(self, metadata={}):
        """
        Construct the workflow. As the workflow is non-stationary, we allocate individual 
        applications and store them within a class.
        """

        MD = {
            'Name': 'Molecule replacement demo workflow',
            'ID': 'RW1',
            'Description': 'Simple workflow to demonstrate molecule replacement in grain state',
            'Version_date': '1.0.0, May 2021',
            'Inputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Grain state',
                 'Description': 'Initial grain state', 'Units': 'None',
                 'Origin': 'Simulated', 'Required': True},
            ],
            'Outputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Dopant state',
                 'Description': 'Dopant', 'Units': 'None',
                 'Origin': 'Simulated', 'Required': True}
            ]
        }

        super().__init__(metadata=MD)
        self.updateMetadata(metadata)

        # model references
        self.createPdbGrain = None
        self.createReplacedState = None
    
    def initialize(self, file='', workdir='', targetTime=0*mp.U.s, metadata={}, validateMetaData=True):
        super().initialize(file=file, workdir=workdir, targetTime=targetTime, metadata=metadata, validateMetaData=validateMetaData)

        self.createPdbGrain = CreatePdb()
        self.createReplacedState = GrainStateFromPDB()

        # To be sure update only required passed metadata in models
        passingMD = {
            'Execution': {
                'ID': self.getMetadata('Execution.ID'),
                'Use_case_ID': self.getMetadata('Execution.Use_case_ID'),
                'Task_ID': self.getMetadata('Execution.Task_ID')
            }
        }

        self.createPdbGrain.initialize(metadata=passingMD, file='vgrain.pdb')

    def set(self, prop, objectID=0):
        if (type(prop) == mp.heavydata.HeavyDataHandle): #todo: test some ID as well
            if prop.id == mp.dataid.MiscID.ID_GrainState:
                self.grainState= prop
        else:
            raise mp.APIError('Unknown property ID')

    def solveStep(self, istep, stageID=0, runInBackground=False):
        
        log.info("Solving workflow")    
        log.debug("Step: %g %g %g" % (istep.getTime().getValue(), istep.getTimeIncrement().getValue(), istep.number))

        try:
            # solve problem 1
            #Get a PDB from the grainState loaded to validate
            self.createPdbGrain.set(self.grainState) #create grain pdb
            self.createPdbGrain.solveStep(istep) #

            #print('Validating', validate('grain.pdb'))
            #We use as input the pdb file
            print('Validating')
            os.system('{} application/validate.py {}'.format(sys.executable, self.workDir+'/'+self.createPdbGrain.file))
            #We have no output from this

            #
            #self.createReplacedState.initialize(metadata=self.metadata, file='replaced.pdb') #writes a new state TODO: I should replace it some how???
            #self.createReplacedState.set(grainStateG)
            #self.createReplacedState.solveStep(istep) #update Grain?

        except mp.apierror.APIError as e:
            log.error("Following API error occurred: %s" % e)

        #self.createGrainState.finishStep(istep)

    def getCriticalTimeStep(self):
        # determine critical time step
        return min(self.createMoleculeInState.getCriticalTimeStep(), self.createGrainState.getCriticalTimeStep(), \
            self.createPdbMoleculeIn.getCriticalTimeStep(), self.createPdbGrain.getCriticalTimeStep(), \
                )#, self.m2.getCriticalTimeStep())

    def terminate(self):
        if self.createGrainState is not None:
            self.createGrainState.terminate()
        super().terminate()
    
    def getApplicationSignature(self):
        return "Example11 workflow 1.0"

    def getAPIVersion(self):
        return "1.0"


if __name__ == '__main__':
    pass