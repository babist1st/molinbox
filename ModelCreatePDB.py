import sys
sys.path.extend(['..', '../..'])
import time, os
import numpy as np

import mupif as mp
from mupif.units import U as u
import logging
log = logging.getLogger()

class CreatePdb (mp.Model):
    """
    Simple model that creates a PDB file
    """
    def __init__(self, metadata={}):
        MD = {
            'Name': 'Application2',
            'ID': 'App2',
            'Description': 'Creates a PDB',
            'Version_date': '02/2019',
            'Physics': {
                'Type': 'Atomistic',
                'Entity': 'Grains'
            },
            'Solver': {
                'Software': 'Python script',
                'Language': 'Python3',
                'License': 'LGPL',
                'Creator': 'Unknown',
                'Version_date': '02/2019',
                'Type': 'Generator',
                'Documentation': 'Nowhere',
                'Estim_time_step_s': 1,
                'Estim_comp_time_s': 0.01,
                'Estim_execution_cost_EUR': 0.01,
                'Estim_personnel_cost_EUR': 0.01,
                'Required_expertise': 'None',
                'Accuracy': 'High',
                'Sensitivity': 'High',
                'Complexity': 'Low',
                'Robustness': 'High'
            },
            'Inputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Grain state',
                 'Description': 'Initial grain state', 'Units': 'None',
                 'Origin': 'Simulated', 'Required': True}
            ],
            'Outputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Grain state',
                 'Description': 'Updated grain state with dopant', 'Units': 'None', 'Origin': 'Simulated'}]
        }
        super().__init__(metadata=MD)
        self.updateMetadata(metadata)
        self.inputGrainState=None
        self.outputGrainState=None

    def initialize(self, file='', workdir='', metadata={}, validateMetaData=True, **kwargs):
        super().initialize(file=file, workdir=workdir, metadata=metadata, validateMetaData=validateMetaData)

    def get(self, propID, time, objectID=0):
        if propID == mp.dataid.MiscID.ID_GrainState:
            return self.outputGrainState
        else:
            raise mp.APIError('Unknown property ID')

    def set(self, prop, objectID=0):
        if (type(prop) == mp.heavydata.HeavyDataHandle): #todo: test some ID as well
            if prop.id == mp.dataid.MiscID.ID_GrainState:
                self.inputGrainState = prop
        else:
            raise mp.APIError('Unknown property ID')

    def solveStep(self, tstep, stageID=0, runInBackground=False):
        # read source grain state, create a PDB file
        t0=time.time()
        atomCounter = 0
        self.outputGrainState=mp.HeavyDataHandle(id=mp.dataid.MiscID.ID_GrainState)
        #log.warning(f'Created temporary {self.outputGrainState.h5path}')
        #outGrains = self.outputGrainState.getData(mode='create',schemaName='grain',schemasJson=mp.heavydata.sampleSchemas_json)
        # readRoot fails if still open
        inGrains = self.inputGrainState.getData(mode='readonly')
        #outGrains.resize(size=len(inGrains))
        f = open(self.file,"w")

        def write_xscfile(cell, filename):
            if cell.value.any() == 0 or np.isnan(cell).any():
                print('Cell Dimension 0: Not Creating xsc for',filename)
                return
            print('Creating xsc file for', filename)
            name = os.path.splitext(os.path.abspath(filename))[0]
            try:
                with open(str(name) + '.xsc', 'w' ) as f:
                    f.write('# NAMD extended system configuration output file\n')
                    f.write('#$LABELS step a_x a_y a_z b_x b_y b_z c_x c_y c_z o_x o_y o_z s_x s_y s_z s_u s_v s_w\n')
                    f.write("0 {} 0 0 0 {} 0 0 {} 0 0 0 0 0 0 0 0 0 0".format(*cell))
            except:
                print('No XSC file',filename)

        cell=(inGrains.getTopology().getCellSize()/u.angstrom).decompose()
        for c in cell:
            #print('Cell size', c)
            write_xscfile(c,self.file)
        #This doesn't work for a grainState with 2 grains, probably normal
        c=0
        for igNum,ig in enumerate(inGrains):
            print(igNum,ig.getMolecules())
            for imNum, im in enumerate(ig.getMolecules()):
                l=len(im.getAtoms())
                for iaNum, ia in enumerate(im.getAtoms()):
                    name = ia.getIdentity().getElement()
                    atx, aty, atz = (ia.getProperties().getTopology().getPosition()/(0.1*u.nm)).decompose()

                    atomCounter+=1
                    f.write("{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}          {:>2s}{:2s}\n".format('ATOM', atomCounter, name, ' ', 'M'+str(l)[0], ' ', imNum+1+c, ' ', atx,aty,atz, 1.00, 0.00, ' ', ' '))
            c=imNum+c
        t1=time.time()
        f.close()
        print(f'{atomCounter} atoms created in {t1-t0:g} sec ({atomCounter/(t1-t0):g}/sec).')

    def getCriticalTimeStep(self):
        return 1.*mp.U.s

    def getAssemblyTime(self, tstep):
        return tstep.getTime()

    def getApplicationSignature(self):
        return "Application1"

