#!/usr/bin/env python

import os, sys, getopt, random
from vmd import molecule, vmdnumpy, atomsel, topology, evaltcl
from pyquaternion import Quaternion
import numpy as np

axis=(  (0,(1,0,0)),
        (1,(0,0,1)),
        (2,(0,1,0))
        )

class Fragment():
    def __init__(self, filename):
        self.name = name = os.path.abspath(filename)
        self.path = os.path.split(os.path.abspath(filename))[0]

        self.molid = molecule.load('pdb', name)
        self.frag = atomsel ( 'all', self.molid)
        self.pos = vmdnumpy.positions(self.molid,0)

        self.conflicts=[]


        self.center_com()

        self.reset_position = np.copy(self.pos)
        self.read_xsc()
        self.resids=list(set(self.frag.resid))
        self.resids.sort()
            
        self.grainresids = {}
        for resname in set(self.frag.resname):
            g = atomsel ( 'resname {0}'.format( resname ), self.molid)
            self.grainresids[resname] = list(set(g.resid))
            self.grainresids[resname].sort()
            print(self.grainresids)

    def get_element(self,resname, resid):
        #f = atomsel ( 'resid {0}'.format(self.resids[resid]), self.molid)
        f = atomsel ( 'resid {0}'.format(self.grainresids[resname][resid]), self.molid)
        return f.name

    def get_number_of_resnames(self, resname):
        f = atomsel ( 'resname {0}'.format(resname), self.molid)
        return len(set(f.resid))

    def get_resname_of_mol(self, resid):
        f = atomsel ( 'resid {0}'.format(self.resids[resid]), self.molid)
        return list(set(f.resname))

    def get_atoms_of_resname(self, resname):
        f = atomsel ( 'resname {0}'.format(resname), self.molid)
        return int(len(f)/len(set(f.resid)))
        #return len(f)

    def get_resid_of_resname(self, resname,resid):
        f = atomsel ( 'resid {0}'.format(self.grainresids[resname][resid]), self.molid)
        return list(set(f.resid))[0]

    def get_atoms_of_molecule(self, resname, resid):
        #f = atomsel ( 'resid {0}'.format(self.resids[resid]), self.molid)
        f = atomsel ( 'resid {0}'.format(self.grainresids[resname][resid]), self.molid)
        print(len(self.resids),resid,self.resids[resid])
        return len(f)

    #def get_position_of_atom(self, resid, index):
    #    sel = vmdnumpy.atomselect( 'index {0}'.format(resid),self.molid, 0)
    def get_position_of_atom(self, resname, resid, index ):
        #f = atomsel ( 'resid {0}'.format(self.resids[resid]), self.molid)
        f = atomsel ( 'resid {0}'.format(self.grainresids[resname][resid]), self.molid)
        #print(f.index[index])
        p = self.get_pos_of_atom(f.index[index])
        return p

    def get_pos_of_atom(self, index):
        sel = vmdnumpy.atomselect( 'index {0}'.format(index),self.molid, 0)
        #print(len(sel))
        sel_ind = np.where(sel==1)
        selection = vmdnumpy.positions(self.molid,0)
        f = selection[sel_ind]
        #print(f[0])
        return f[0]

    def center_com(self):
        self.find_com()
        self.move_com_to_zero()

    def temp_save(self):
        self.temp_pos = np.copy(self.pos)

    def temp_reset(self):
        self.pos[:] = self.temp_pos[:]

    def get_minmax(self):
        self.minim, self.maxim = np.asarray(self.frag.minmax())

    def move_com_to_zero (self):
        self.frag.update()
        self.com = self.frag.center(weight=self.frag.mass)
        v = tuple([-1*x for x in self.com])
        self.frag.moveby(v)
        self.com = self.frag.center(weight=self.frag.mass)
        self.comar = np.array(self.com)

    def find_com(self):
        try:
            self.com = self.frag.center(weight=self.frag.mass)
        except:
            print('Guesing mass 1')
            self.frag.mass=[1]
            self.com = self.frag.center(weight=self.frag.mass)
        self.comar = np.array(self.com)

    def reset(self):
        self.pos[:] = self.reset_position[:]
    
    def read_xsc(self):
        try:
            name = os.path.splitext(os.path.abspath(self.name))[0]
            with open(str(name) + '.xsc') as f:
                f.readline()
                f.readline()
                line = f.readline()
                s = line.split(" ")
                self.box = np.asarray( [ float(s[1]), float(s[5]), float(s[9])] )
            a, b, c = self.box
            molecule.set_periodic(self.molid, a=a, b=b, c=c)
        except:
            pass
            #print("MISSING XSC FILE",name)
            #minim, maxim = np.asarray(self.frag.minmax())
            #self.box = vec_sub(minim , maxim)
            #self.box = maxim-minim

    def validate( self, contact = 1 ):
        self.contact = contact
        residues=set(self.frag.resid)
        state = self.collect_overlaps(self.frag)
        #for resid in residues:
        #    sel = atomsel ( 'resid {0}'.format(resid), self.molid)
        #    print(resid)
        #    state = self.pbc(sel)
        #    if state:
        #        break
        n = random.uniform(0,1) #for demonstration purposes
        if n <0.9:
            state = False
        else:
            state = True
        return state

    def collect_overlaps(self,sel):
        self.contacts(sel)
        try:
            a = ' '.join(str(n) for n in self.conflicts)
            overlap = atomsel ('same resid as index {}'.format(a),self.molid)

            l = set(overlap.resid)
            print('Residues overlap', len(l), l)
            self.selection = atomsel ('not same resid as index {}'.format(a),self.molid)
            return 1
        except ValueError as e:
            #print('No overlap')
            return 0

    def contacts (self, sel):
        mol, pet = sel.contacts(self.frag, self.contact)
        self.conflicts += set(pet)
        self.l = len(self.conflicts)

    def pbc (self,sel):
        pbc = [[0,0,0]]
        #pbc= [ [0,0,0], [-1,0,0], [-1,-1,0], [-1,-1,-1], [0,-1,-1], [-1,0,-1], [0,-1,0], [0,0,-1], [1,0,0], [1,1,0], [1,1,1], [0,1,1], [0,1,0], [1,0,1], [0,0,1]]
        #pbc= [ [0,0,0], [-1,0,0], [0,-1,0], [0,0,-1], [1,0,0], [0,1,0], [0,0,1]]
        self.temp_save()
        for m in pbc:
            self.frag.moveby(-np.asarray(m)*self.box)
            state = self.collect_overlaps(sel)
            self.temp_reset()
        return state

    def write ( self ,sel = None):
        try:
            self.selection.update()
            molecule.write(self.molid, "pdb", self.path+"/replaced.pdb",0,-1,1, self.selection)
        except AttributeError as e: 
            print('PRINTING EVERYTHING',e)
            self.selection = self.frag #atomsel ( 'all', self.molid)
            molecule.write(self.molid, "pdb", self.path+"/test.pdb",0,-1,1, self.selection)
        return self.path+"/replaced.pdb"

    def fix_resid(self):
        p = np.array(self.selection.chain)
        resid = np.array(self.selection.resid)
        beta = np.array(self.selection.beta)
        segid = np.array(self.selection.segid)

        ar = beta.astype(int)

class RepBox(Fragment):
    def __init__(self, contact = 2, replacements = 0, grain = None, replicas = []):
        self.cpylist = ('name','type','mass','charge','radius','resid', 'element', 'resname','chain','segname')
        self.l=None

        self.grain = grain
        self.replicas = replicas
        self.path=self.grain.path
        if replicas:
            for r in replicas:
                replicans = len(self.replicas)*len(r.frag)
                self.natoms = replicans + len(self.grain.frag)
            self.create_box()
        self.pos = vmdnumpy.positions(self.molid,0)

        self.frag = grain.frag
        self.box = grain.box
        self.contact = contact

        self.conflicts = []
        self.l = []

        self.replicate_everything()

    def create_box( self ):
        molname = 'replication'
        self.molid = molecule.new(molname, self.natoms)
        molecule.dupframe(self.molid, -1)

    def find_optimal( self , rotate = False):
        res=None
        contacts=len(set(self.grain.frag.resid))
        print('Contacts',contacts)
        for resid in range(1,len(set(self.grain.frag.resid))+1):
            self.conflicts=[]
            angle=np.array([0,0,0])
            if rotate:
                for rotx in range(0,180):
                    for roty in range(0,180):
                        for rotz in range(0,180):
                            angle=[rotx,roty,rotz]
                            self.replace(resid, angle=angle)
                            print(angle,contacts, res)
                            if self.l < contacts:
                                contacts = self.l
                                res = resid
                            if contacts == 0:
                                return contacts, res
                            f=self.replicas[0]
                            f.reset()
            else:
                self.replace(resid, angle=angle)
            print(resid, angle)
            if self.l < contacts:
                contacts = self.l
                res = resid
            if contacts == 0:
                print('0',contacts, res)
                return contacts, res

        print('yp',contacts, res)
        return contacts, res

    def replace( self, resid, angle = [0,0,0]):
        f = self.replicas[0]
        sel = atomsel ( 'resid {0}'.format(resid), self.molid)
        sel = self.rotate(resid, angle = angle)
        sel, mobile = self.move_residue(sel)

    def rotate(self, resid, angle = [0, 0, 0]):
        sel = atomsel ( 'resid {0}'.format(resid), self.molid)
        if any(angle):
            f = self.replicas[0]
            mobile = atomsel ( self.mobile, self.molid )
            for i, ax in axis:
                M=Quaternion(axis=list(ax), degrees=-angle[i])
                Mt=tuple(M.transformation_matrix.ravel())
                mobile.move(Mt)
                f.frag.move(Mt)
            f.reset()
        return sel

    def move_residue(self,sel):
        #Move com of atoms to the bead and reset frag
        f = self.replicas[0]
        com = np.asarray(sel.center(weight = sel.mass))
        box = self.grain.box/2

        v = com
        f.frag.moveby(v)
        mobile = atomsel ( self.mobile, self.molid )
        mobile.moveby(v)
        self.pbc_overlap(f.frag)

        f.reset()
        return sel, mobile

    def pbc_overlap( self, sel ):
        print('Checking PBC',sel)
        f = self.replicas[0]
        box = self.grain.box
        pbc= [ [0,0,0], [-1,0,0], [-1,-1,0], [-1,-1,-1], [0,-1,-1], [-1,0,-1], [0,-1,0], [0,0,-1], [1,0,0], [1,1,0], [1,1,1], [0,1,1], [0,1,0], [1,0,1], [0,0,1]]
        #pbc= [ [0,0,0], [-1,0,0], [0,-1,0], [0,0,-1], [1,0,0], [0,1,0], [0,0,1]]
        if pbc:
            f.temp_save()
            for m in pbc:
                f.frag.moveby(-np.asarray(m)*box)
                state = self.collect_overlaps(sel)
                f.temp_reset()
        state = self.collect_overlaps(sel)
        return state

    def replicate_everything(self):
        f = self.replicas[0]
        ntotal = len(self.grain.frag)
        frag_atoms = len(f.frag)

        selection = 'index {0} to {1}'.format( 0, ntotal -1)
        newsel = atomsel ( selection, self.molid )
        
        #Copy attributes and positions to the empty atoms
        newsel = self.copy_attributes_from( self.grain.frag, newsel )
        self.pos[0:ntotal] = self.grain.pos[:]

        self.mobile = 'index {0} to {1}'.format( ntotal, ntotal + frag_atoms-1)
        newsel = atomsel ( self.mobile, self.molid )
        newsel = self.copy_attributes_from( f.frag, newsel )
        newsel.resid=len(set(self.grain.frag.resid))+1
        self.pos[ntotal:ntotal+frag_atoms+1] = f.pos[:]

        #self.frag = atomsel ( 'all', self.molid)
        #self.selection = atomsel('all',self.molid)
        
    def copy_attributes_from( self, sel, newsel ):
        for attribute in self.cpylist:
            # newsel.attribute = sel.attribute
            val = sel.get(attribute)
            newsel.set( attribute, val)
        val = sel.resid
        return newsel

def main():
    grain = Fragment('boxht8.pdb')
    f1 = Fragment('icba12.pdb')

    box = RepBox ( contact = 1, grain = grain , replicas = [f1] )
    box.replace( 19, angle=[0,0,0])
 
    box.write()

if __name__ == "__main__":
    main()
