#!/usr/bin/env python

import sys
from box import Fragment

def validate(grainfile,c=1):
    grain = Fragment(grainfile)
    state = grain.validate(c)
    return state

if __name__ == "__main__":
    print('gogogo validate')
    try:
        pdbBox = sys.argv[1]
    except:
        print('No grain and molecule files passed as arguments')
        sys.exit(2)
    s = validate( pdbBox )
    print('done validating',s)
