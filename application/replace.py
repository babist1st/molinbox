#!/usr/bin/env python

from box import *

def replace(grainfile, fragfile, molecule, c=1):
    grain = Fragment(grainfile)
    f1 = Fragment(fragfile)

    box = RepBox ( contact = c, grain = grain , replicas = [f1] )
    box.replace( molecule )
    pdbfile = box.write()
    return pdbfile

if __name__ == "__main__":
    try:
        grain = sys.argv[1]
        molecule = sys.argv[2]
        resid = sys.argv[3]
    except:
        print('No grain/molecule files or resid to replace are passed as arguments')
        sys.exit(2)
    #pdb = replace('boxht8.pdb','icba12.pdb',19,1)
    pdb = replace( grain, molecule ,resid )
