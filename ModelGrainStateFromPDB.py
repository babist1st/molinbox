import sys, os
sys.path.extend(['..', '../..'])
import time

import mupif as mp
from mupif.units import U as u
import logging
log = logging.getLogger()
from box import *


class GrainStateFromPDB (mp.Model):
    """
    Simple model that uploads an existing PDB file and if it exists the PBC (xsc file) of the box
    """
    def __init__(self, metadata={}):
        MD = {
            'Name': 'Upload PDB',
            'ID': 'uPDB',
            'Description': 'Uploads existing PDB file',
            'Version_date': '06/2021',
            'Physics': {
                'Type': 'Atomistic',
                'Entity': 'Grains'
            },
            'Solver': {
                'Software': 'Python script',
                'Language': 'Python3',
                'License': 'LGPL',
                'Creator': 'Unknown',
                'Version_date': '06/2021',
                'Type': 'Generator',
                'Documentation': 'Nowhere',
                'Estim_time_step_s': 1,
                'Estim_comp_time_s': 0.01,
                'Estim_execution_cost_EUR': 0.01,
                'Estim_personnel_cost_EUR': 0.01,
                'Required_expertise': 'None',
                'Accuracy': 'High',
                'Sensitivity': 'High',
                'Complexity': 'Low',
                'Robustness': 'High'
            },
            'Inputs': [],
            'Outputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Grain state',
                 'Description': 'Sample Random grain state', 'Units': 'None', 'Origin': 'Simulated'}]
        }
        super().__init__(metadata=MD)
        self.updateMetadata(metadata)
        self.grainState=None

    def initialize(self, file='', workdir='', metadata={}, validateMetaData=True, **kwargs):
        super().initialize(file=file, workdir=workdir, metadata=metadata, validateMetaData=validateMetaData)

    def get(self, propID, time, objectID=0):
        

        if propID == mp.dataid.MiscID.ID_GrainState:
            return self.grainState
        else:
            raise mp.APIError('Unknown property ID')

    def setProperty(self, prop, objectID=0):
        raise mp.APIError('Unknown property ID')

    def solveStep(self, tstep, stageID=0, runInBackground=False):
        # Read PDB
        f = Fragment(self.workDir+'/'+self.file)
        #print(self.workDir)
        print('Creating grain state',self.file)

        t0=time.time()
        atomCounter=0
        #print(self.grainState)
        try:
            grains=self.grainState.getData(mode='create',schemaName='grain',schemasJson=mp.heavydata.sampleSchemas_json)
        except:
            self.grainState=mp.heavydata.HeavyDataHandle(id=mp.dataid.MiscID.ID_GrainState)
            grains=self.grainState.getData(mode='create',schemaName='grain',schemasJson=mp.heavydata.sampleSchemas_json)
        grains.resize(size=len(set(f.frag.resname)))

        #Get number of unique molecule names, basically the number of grains
        print('Residue name', set(f.frag.resname))
        resname=list(set(f.frag.resname))

        #Extract cell size to xsc format
        try:
            grains.getTopology().setCellSize(f.box*u.angstrom)
            b=grains.getTopology().getCellSize()
            print('Cell size', b)
        except:
            print('No box size info')

        # Write PDB to grainstate
        for ig,g in enumerate(grains):
            mols = f.get_number_of_resnames(resname[ig])
            g.getMolecules().resize(size=mols)
            print(f"Grain #{ig} has {mols} molecules")
            for im, m in enumerate(g.getMolecules()):
                m.getAtoms().resize(size=f.get_atoms_of_resname(resname[ig]))
                el = f.get_element(resname[ig],im)
                #rn = f.get_resname_of_mol(im)
                #m.getIdentity().setChemicalName(rn*u.nm) #TODO: I NEED a molecule/res name to put it in
                for ia, a in enumerate(m.getAtoms()):
                    p = f.get_position_of_atom(resname[ig],im,ia)
                    a.getIdentity().setElement(el[ia][0])
                    a.getProperties().getTopology().setPosition(p)
                    atomCounter+=1
        self.grainState.closeData()
        t1=time.time()
        print(f'{atomCounter} atoms created in {t1-t0:g} sec ({atomCounter/(t1-t0):g}/sec).')
        md = {
                'Execution': {
                    'ID': self.getMetadata('Execution.ID'),
                    'Use_case_ID': self.getMetadata('Execution.Use_case_ID'),
                    'Task_ID': self.getMetadata('Execution.Task_ID')
                }
            }
        self.grainState.updateMetadata(md)

    def getCriticalTimeStep(self):
        return 1.*mp.U.s

    def getAssemblyTime(self, tstep):
        return tstep.getTime()

    def getApplicationSignature(self):
        return "Application1"