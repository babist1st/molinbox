import sys, os, random
sys.path.extend(['..', '../..', 'application'])

import mupif as mp
from mupif.units import U as u
import logging
log = logging.getLogger()

from ModelGrainStateFromPDB import GrainStateFromPDB
from ModelCreatePDB import CreatePdb
from replace import *

#class WorkflowReplace (mp.Model):
class WorkflowReplace (mp.workflow.Workflow):
    """
    Simple model that replaces random molecule in grain by another one (dopant)
    """
    def __init__(self, metadata={}):
        MD = {
            'Name': 'Application2',
            'ID': 'App2',
            'Description': 'Replaces random molecule in grain by another one',
            'Version_date': '02/2019',
            'Physics': {
                'Type': 'Atomistic',
                'Entity': 'Grains'
            },
            'Solver': {
                'Software': 'Python script',
                'Language': 'Python3',
                'License': 'LGPL',
                'Creator': 'Unknown',
                'Version_date': '02/2019',
                'Type': 'Generator',
                'Documentation': 'Nowhere',
                'Estim_time_step_s': 1,
                'Estim_comp_time_s': 0.01,
                'Estim_execution_cost_EUR': 0.01,
                'Estim_personnel_cost_EUR': 0.01,
                'Required_expertise': 'None',
                'Accuracy': 'High',
                'Sensitivity': 'High',
                'Complexity': 'Low',
                'Robustness': 'High'
            },
            'Inputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'State',
                 'Description': 'State', 'Units': 'None',
                 'Origin': 'Simulated', 'Required': True, 'Obj_ID': ['grain', 'dopant']}
            ],
            'Outputs': [
                {'Type': 'mupif.GrainState', 'Type_ID': 'mupif.MiscID.ID_GrainState', 'Name': 'Dopant state',
                 'Description': 'Dopant', 'Units': 'None',
                 'Origin': 'Simulated'}
            ]
        }
        super().__init__(metadata=MD)
        self.updateMetadata(metadata)
        self.createPdbMoleculeIn = None
        self.createPdbGrain = None
        self.createReplacedState = None
        self.outputGrainState = None
        self.grainStateS = None
        self.grainStateG = None

    def initialize(self, file='', workdir='', metadata={}, validateMetaData=True, **kwargs):
        super().initialize(file=file, workdir=workdir, metadata=metadata, validateMetaData=validateMetaData)
        self.createPdbMoleculeIn = CreatePdb()
        self.createPdbGrain = CreatePdb()
        self.createReplacedState = GrainStateFromPDB()

        passingMD = {
            'Execution': {
                'ID': self.getMetadata('Execution.ID'),
                'Use_case_ID': self.getMetadata('Execution.Use_case_ID'),
                'Task_ID': self.getMetadata('Execution.Task_ID')
            }
        }
        #Hard coded names for the input pdb files, these can't bre changed
        self.createPdbMoleculeIn.initialize(metadata=passingMD, file='MoleculeIn.pdb')
        self.createPdbGrain.initialize(metadata=passingMD, file='grain.pdb')

    def get(self, objectTypeID, time=None, objectID=0):
        if objectTypeID == mp.dataid.MiscID.ID_GrainState:
            return self.outputGrainState
        else:
            raise mp.APIError('Unknown property ID')

    def set(self, obj, objectID=0):
        if type(obj) == mp.heavydata.HeavyDataHandle and objectID == 'grain':
            if obj.id == mp.dataid.MiscID.ID_GrainState:
                self.grainStateG = obj
        elif type(obj) == mp.heavydata.HeavyDataHandle and objectID == 'dopant':
            if obj.id == mp.dataid.MiscID.ID_GrainState:
                self.grainStateS = obj
        else:
            raise mp.APIError('Unknown property ID')

    def solveStep(self, istep, stageID=0, runInBackground=False):
        #Feed the two grainStates and
        #basicly create my input files which are:
        #MoleculeIn.pdb, grain.pdb and grain.xsc
        self.createPdbGrain.set(self.grainStateG) 
        self.createPdbMoleculeIn.set(self.grainStateS) 

        #Actually create the pdb files
        #TODO: The current class should be just a model (not a workflow) and the two bellow a method
        self.createPdbGrain.solveStep(istep) #get grain output to pdb
        self.createPdbMoleculeIn.solveStep(istep) #get mol output to pdb

        #try:
        #Run the application that will replace a random molecule, the result is a pdb file
        #replace(self.workDir+'/'+self.createPdbGrain.file, self.workDir+'/'+self.createPdbMoleculeIn.file, 100)
        os.system('{} application/replace.py {} {} {}'.format(sys.executable,self.workDir+'/'+self.createPdbGrain.file, self.workDir+'/'+self.createPdbMoleculeIn.file, random.randint(1,100)))

        #creates a new state from the replaced pdb 
        # TODO: I should replace the already existing state somehow??? A lot I don't understad still, currently not important
        self.createReplacedState.initialize(metadata=self.metadata, file='replaced.pdb') 
        self.createReplacedState.solveStep(istep)
        self.outputGrainState = self.createReplacedState.get(mp.dataid.MiscID.ID_GrainState, self.createReplacedState.getAssemblyTime(istep))
        #except:
        #    log.error('Failed to execute replacement')

    def getCriticalTimeStep(self):
        return 1.*mp.U.s

    def getAssemblyTime(self, tstep):
        return tstep.getTime()

    def getApplicationSignature(self):
        return "Application1"

