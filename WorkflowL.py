import sys
sys.path.extend(['.','..', '../..','application'])

import mupif as mp
from mupif.units import U as u
import logging
log = logging.getLogger()

from ModelGrainStateFromPDB import GrainStateFromPDB
from WorkflowReplace import WorkflowReplace
from WorkflowValidate import WorkflowValidate


class Example11(mp.workflow.Workflow):
   
    def __init__(self, metadata={}):
        """
        Construct the workflow. As the workflow is non-stationary, we allocate individual 
        applications and store them within a class.
        """

        MD = {
            'Name': 'Molecule replacement demo workflow',
            'ID': 'RW1',
            'Description': 'Simple workflow to demonstrate molecule replacement in grain state',
            'Version_date': '1.0.0, May 2021',
            'Inputs': [],
            'Outputs': []
        }

        super().__init__(metadata=MD)
        self.updateMetadata(metadata)

        # model references
        self.replacer = None
        self.validator = None
    
    def initialize(self, file='', workdir='', targetTime=0*mp.U.s, metadata={}, validateMetaData=True):
    
        super().initialize(file=file, workdir=workdir, targetTime=targetTime, metadata=metadata, validateMetaData=validateMetaData)
        self.createMoleculeInState = GrainStateFromPDB()
        self.createGrainState = GrainStateFromPDB()
        self.replacer = WorkflowReplace()
        self.validator = WorkflowValidate()
        #self.createValidatedState = GrainStateFromPDB()

        # To be sure update only required passed metadata in models
        passingMD = {
            'Execution': {
                'ID': self.getMetadata('Execution.ID'),
                'Use_case_ID': self.getMetadata('Execution.Use_case_ID'),
                'Task_ID': self.getMetadata('Execution.Task_ID')
            }
        }

        #Hard coded names for the files that will create the initial state, these could be easily removed
        self.createGrainState.initialize(metadata=passingMD, file='prep/boxht8.pdb')
        self.createMoleculeInState.initialize(metadata=passingMD, file='prep/icba12.pdb')
        self.replacer.initialize(metadata=passingMD)
        self.validator.initialize(metadata=passingMD)

    def solveStep(self, istep, stageID=0, runInBackground=False):
        
        log.info("Solving workflow")    
        log.debug("Step: %g %g %g" % (istep.getTime().getValue(), istep.getTimeIncrement().getValue(), istep.number))
        

        try:
            # I need a grain and a molecule
            # Based on Example11 create 2 grain states from pdb files
            self.createGrainState.solveStep(istep) #read grain pdb
            self.createMoleculeInState.solveStep(istep) #read molecule pdb

            # handshake the data
            grainStateG = self.createGrainState.get(mp.dataid.MiscID.ID_GrainState, self.createGrainState.getAssemblyTime(istep))
            grainStateM = self.createMoleculeInState.get(mp.dataid.MiscID.ID_GrainState, self.createMoleculeInState.getAssemblyTime(istep))

            # solve problem 1
            #Feed the states to the Model/Workflow that replaces one random molecule.
            self.replacer.set(obj=grainStateG, objectID='grain')
            self.replacer.set(obj=grainStateM, objectID='dopant')
            self.replacer.solveStep(istep)

            #Get the replaced grain state
            grainStateReplaced = self.replacer.get(mp.dataid.MiscID.ID_GrainState, self.replacer.getAssemblyTime(istep))

            # solve problem 2
            #Feed the replaced state to the Model/Workflow that validates
            self.validator.set(grainStateReplaced)
            self.validator.solveStep(istep)

            #At this point I should update the original with a new validated state

            #grainValidated = self.validator.get(mp.dataid.MiscID.ID_GrainState, self.validator.getAssemblyTime(istep))

            #self.uploadValidated.initialize(metadata=self.metadata, file='replaced.pdb') #writes a new state TODO: I should replace it some how???
            ##self.createReplacedState.set(grainStateG)
            ##grainState2 = self.m2.get(mp.dataid.MiscID.ID_GrainState, self.createMoleculeInState.getAssemblyTime(istep))
            #self.uploadValidated.solveStep(istep) #update Grain?

        except mp.apierror.APIError as e:
            log.error("Following API error occurred: %s" % e)

        self.createMoleculeInState.finishStep(istep)
        self.createGrainState.finishStep(istep)

    def getCriticalTimeStep(self):
        # determine critical time step
        return min(self.createMoleculeInState.getCriticalTimeStep(), self.createGrainState.getCriticalTimeStep())

    def terminate(self):
        if self.createMoleculeInState is not None:
            self.createMoleculeInState.terminate()
        if self.createGrainState is not None:
            self.createGrainState.terminate()
        if self.replacer is not None:
            self.replacer.terminate()
        super().terminate()
    
    def getApplicationSignature(self):
        return "Example11 workflow 1.0"

    def getAPIVersion(self):
        return "1.0"


if __name__ == '__main__':
    workflow = Example11()
    workflowMD = {
        'Execution': {
            'ID': '1',
            'Use_case_ID': '1_1',
            'Task_ID': '1'
        }
    }
    workflow.initialize(targetTime=1*mp.U.s, metadata=workflowMD)
    workflow.solve()
    workflow.terminate()
